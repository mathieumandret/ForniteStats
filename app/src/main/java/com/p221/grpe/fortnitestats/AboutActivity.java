package com.p221.grpe.fortnitestats;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.webkit.WebView;
import android.widget.Button;

public class AboutActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setLayout(R.layout.content_apropos);

        // Définition du titre de l'activité
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setTitle(R.string.activityAbout);
        }

        // Web Client pour ouvrir le navigateur de l'utilisateur
        CustomWebViewClient wvc = new CustomWebViewClient(this);

        Button linkapi = findViewById(R.id.linkApi);
        Button githubJ = findViewById(R.id.githubJules);
        Button githubM = findViewById(R.id.githubMathieu);

        // Écouteur sur le bouton de l'API
        linkapi.setOnClickListener(view -> {
            wvc.shouldOverrideUrlLoading(new WebView(this), "https://fortnitetracker.com/site-api");
        });

        // Écouteur sur le bouton GitHub de Jules
        githubJ.setOnClickListener(view -> wvc.shouldOverrideUrlLoading(new WebView(this), "https://github.com/JulesLassara"));

        // Écouteur sur le bouton GitHub de Mathieu
        githubM.setOnClickListener(view -> wvc.shouldOverrideUrlLoading(new WebView(this), "https://github.com/MathieuMandret"));

    }

}
