package com.p221.grpe.fortnitestats;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.ProgressBar;
import android.widget.Toast;

import static com.p221.grpe.fortnitestats.ThemeUtils.LIGHT_THEME;

/**
 * Activite de base contenant le drawer, la toolbar et le menu
 * elle doit être héritée par toutes activités qui veulent partager
 * le drawer, la toolbar et le menu
 */
public abstract class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String BASE_URL = "https://api.fortnitetracker.com/v1/profile/%s/%s";
    public static final String ID = "com.p221.grpe.fornitestats";
    public static final String PREF_ID_THEME = "theme_selected";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Definition du theme
        ThemeUtils.onActivityCreateSetTheme(this);

        setContentView(R.layout.activity_base);
        // Mettre en place la barre de navigation
        // et le menu lateral
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, 0, 0);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    /**
     * Ferme menu quand on clique sur retour
     */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // Definition du texte de l'option de changement de theme
        // en fonction du theme actif
        MenuItem item = menu.findItem(R.id.item_theme);
        if (item != null) {
            item.setTitle(((ThemeUtils.getActiveTheme() == LIGHT_THEME) ? getString(R.string.item_theme_nuit) : getString(R.string.item_theme_jour)));
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_theme:
                // Changement de thème
                ThemeUtils.switchTheme(this);
                break;
            case R.id.item_a_propos:
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.item_quitter:
                this.finishAffinity();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_recherche:
                // Si on est pas deja dans l'activite, y aller
                if (this.getClass() != MainActivity.class) {
                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                break;
            case R.id.item_favoris:
                if (this.getClass() != FavoritesActivity.class) {
                    Intent intent = new Intent(this, FavoritesActivity.class);
                    startActivity(intent);
                    finish();
                }
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Utilise la layout d'id donné
     *
     * @param id identifiant du layout a utiliser
     */
    protected void setLayout(int id) {
        ViewStub stub = findViewById(R.id.layout_stub);
        stub.setLayoutResource(id);
        stub.inflate();
    }


    /**
     * Affiche un message dans un toast
     *
     * @param message Message a afficher
     */
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Affiche la barre de chargement
     */
    public void showLoading() {
        ProgressBar progress = findViewById(R.id.progressBar);
        progress.setVisibility(View.VISIBLE);
    }

    /**
     * Cache la barre de chargement
     */
    public void hideLoading() {
        ProgressBar progress = findViewById(R.id.progressBar);
        progress.setVisibility(View.INVISIBLE);
    }

    /**
     * Lance l'activite PlayerStatsActivity
     *
     * @param result json non decode a passer a l'activite
     */
    public void onSuccessfulGet(String result) {
        Intent intent = new Intent(this, PlayerStatsActivity.class);
        intent.putExtra(BaseActivity.ID, result);
        this.startActivity(intent);
    }
}
