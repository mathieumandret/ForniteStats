package com.p221.grpe.fortnitestats;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;


public class CompareFragment extends Fragment {

    private Activity parent_activity;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.parent_activity = getActivity();
        if (parent_activity == null) return;
        // Lancer la selection du favori lors du clic sur le bouton
        Button btn = parent_activity.findViewById(R.id.compare_button);
        btn.setOnClickListener(v -> {
            Intent pickFavIntent = new Intent(this.parent_activity, PickFavoriteActivity.class);
            this.startActivityForResult(pickFavIntent, PlayerStatsActivity.PICK_FAV);
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_compare, container, false);
    }

    /**
     * Genere une ArrayList contenant les statistiques de 2 joueurs
     *
     * @param player1 premier joueur a comparer
     * @param player2 second joueur a comparer
     * @return Liste de statistiques
     */
    public ArrayList<StatPair> generateComparisonArray(Player player1, Player player2) {
        ArrayList<StatPair> stats_list = new ArrayList<>();
        stats_list.add(new StatPair<>(player1.getWins(), player2.getWins(), R.id.row_top1));
        stats_list.add(new StatPair<>(player1.getWinrate(), player2.getWinrate(), R.id.row_winrate));
        stats_list.add(new StatPair<>(player1.getGames_played(), player2.getGames_played(), R.id.row_game_played));
        stats_list.add(new StatPair<>(player1.getTotal_kills(), player2.getTotal_kills(), R.id.row_total_kills));
        stats_list.add(new StatPair<>(player1.getKd(), player2.getKd(), R.id.row_kd));
        stats_list.add(new StatPair<>(player1.getKills_per_match(), player2.getKills_per_match(), R.id.row_kill_per_match));
        stats_list.add(new StatPair<>(player1.getAvg_survival_time(), player2.getAvg_survival_time(), R.id.row_avg_survival));
        stats_list.add(new StatPair<>(player1.getTime_played(), player2.getTime_played(), R.id.row_time_played));
        stats_list.add(new StatPair<>(player1.getXp(), player2.getXp(), R.id.row_xp));
        return stats_list;
    }

    /**
     * Remplit le tableau de comparaison des statistiques de 2 joueurs
     *
     * @param p1_name Nom du premier joueur
     * @param p2_name Nom du second joueur
     * @param data    Donne a mettre dans le tableau
     */
    public void fillTable(String p1_name, String p2_name, ArrayList<StatPair> data) {
        if (this.parent_activity == null) return;
        TextView col_p1 = this.parent_activity.findViewById(R.id.column_p1);
        TextView col_p2 = this.parent_activity.findViewById(R.id.column_p2);
        col_p1.setText(p1_name);
        col_p2.setText(p2_name);
        for (StatPair stat : data) {
            TableRow row = this.parent_activity.findViewById(stat.getViewId());
            TextView tv_p1 = (TextView) row.getChildAt(1);
            TextView tv_p2 = (TextView) row.getChildAt(2);
            tv_p1.setText(stat.getP1_stat().toString());
            tv_p2.setText(stat.getP2_stat().toString());
        }
    }
}
