package com.p221.grpe.fortnitestats;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class CustomWebViewClient extends WebViewClient {

    private Context context;

    public CustomWebViewClient(Context context) {
        this.context = context;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        this.context.startActivity(i);
        return true;
    }

}
