package com.p221.grpe.fortnitestats;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract;

import java.util.ArrayList;

public class Database {

    private static Database instance;
    private DatabaseManager dbm;
    private SQLiteDatabase db;

    private Database(Context context) {
        dbm = new DatabaseManager(context);
    }

    /**
     * <b>Singleton de la base de donnée</b>
     *
     * @param context
     * @return DatabaseManager
     */
    public static Database getInstance(Context context) {
        if (instance == null) {
            instance = new Database(context);
        }
        return instance;
    }

    /**
     * <b>Ajoute un favoris à la base de données</b>
     *
     * @param player le joueur à ajouter
     */
    public void ajouterFavoris(Player player) {
        if (!player.isFavorite()) {
            this.db = this.dbm.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(DatabaseManager.TABLE_FAVORIS_PSEUDO, player.getName());
            values.put(DatabaseManager.TABLE_FAVORIS_PLATEFORME, player.getPlatform().getSyntax());
            // Insertion
            this.db.insert(DatabaseManager.TABLE_FAVORIS, null, values);
            this.db.close();
        }
    }

    /**
     * <b>Retire un favoris de la base de données</b>
     *
     * @param player le joueur à retirer
     */
    public void removeFavorite(Player player) {
        if (player.isFavorite()) {
            this.db = this.dbm.getWritableDatabase();
            String whereClause =  String.format("%s = ? and %s = ?",
                    DatabaseManager.TABLE_FAVORIS_PSEUDO,
                    DatabaseManager.TABLE_FAVORIS_PLATEFORME
            );
            String[] params = {
                    player.getName(),
                    player.getPlatform().getSyntax()
            };
            this.db.delete(DatabaseManager.TABLE_FAVORIS, whereClause, params);
        }
    }

    public boolean playerIsFavorite(Player player) {
        this.db = this.dbm.getReadableDatabase();
        String[] params = {
                player.getName(),
                player.getPlatform().getSyntax()
        };
        String query = String.format("SELECT %s FROM %s WHERE %s = ? AND %s = ?",
                DatabaseManager.TABLE_FAVORIS_PSEUDO,
                DatabaseManager.TABLE_FAVORIS,
                DatabaseManager.TABLE_FAVORIS_PSEUDO,
                DatabaseManager.TABLE_FAVORIS_PLATEFORME);
        Cursor cursor = this.db.rawQuery(query, params);
        // On pourrait utiliser return cursor.moveToFirst(); mais il faut pouvoir appeler cursor.close()
        if (cursor.moveToFirst()) {
            cursor.close();
            return true;
        } else {
            cursor.close();
            return false;
        }
    }

    public ArrayList<Player> getAllFavoris() {
        this.db = this.dbm.getReadableDatabase();

        ArrayList<Player> favoris = new ArrayList<>();
        String[] cols = {DatabaseManager.TABLE_FAVORIS_PSEUDO, DatabaseManager.TABLE_FAVORIS_PLATEFORME};
        String[] select = {};
        String pseudo;
        Platform platform;

        Cursor cursor = this.db.query(DatabaseManager.TABLE_FAVORIS, cols, "", select, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                pseudo = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseManager.TABLE_FAVORIS_PSEUDO));
                platform = Platform.getEnumByString(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseManager.TABLE_FAVORIS_PLATEFORME)));

                favoris.add(new Player(pseudo, platform));
            } while (cursor.moveToNext());
        }

        return favoris;

    }

}
