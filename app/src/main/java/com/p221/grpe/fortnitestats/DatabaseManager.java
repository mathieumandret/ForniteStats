package com.p221.grpe.fortnitestats;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseManager extends SQLiteOpenHelper {

    // Informations Database
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "favoris.db";

    // Table Database
    public static final String TABLE_FAVORIS = "favoris";

    // Colonnes table favoris
    public static final String TABLE_FAVORIS_PSEUDO = "pseudo";
    public static final String TABLE_FAVORIS_PLATEFORME = "plateforme";

    // Création de la table favoris
    private final String SQL_CREATE = "CREATE TABLE " + DatabaseManager.TABLE_FAVORIS + " (" +
                                      DatabaseManager.TABLE_FAVORIS_PSEUDO + " VARCHAR(255), " +
                                      DatabaseManager.TABLE_FAVORIS_PLATEFORME + " VARCHAR(4), " +
                                      "CONSTRAINT PK_Favoris PRIMARY KEY(pseudo, plateforme));";

    // Suppression de la table favoris
    private final String SQL_DELETE = "DROP TABLE IF EXISTS " + DatabaseManager.TABLE_FAVORIS + ";";

    public DatabaseManager(Context context) {
        super(context, DatabaseManager.DATABASE_NAME, null, DatabaseManager.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(this.SQL_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {
        db.execSQL(this.SQL_DELETE);
        this.onCreate(db);
    }

}
