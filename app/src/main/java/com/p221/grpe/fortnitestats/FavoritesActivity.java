package com.p221.grpe.fortnitestats;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBar;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;


public class FavoritesActivity extends BaseActivity {

    private ArrayList<HashMap<String, String>> favoritesList;
    private SimpleAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setLayout(R.layout.content_favoris);
        // Définition du titre de l'activité
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setTitle(R.string.activityFav);
        }

        // Mise à jour de l'élément du menu actif
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(1).setChecked(true);

        ListView favoritesView = findViewById(R.id.list_favoris);

        this.favoritesList = new ArrayList<>();
        // Creer l'adapteur qui associe les donnee a la ListView
        this.adapter = new SimpleAdapter(this, this.favoritesList,
                android.R.layout.simple_list_item_2,
                new String[]{"pseudo", "platform"},
                new int[]{android.R.id.text1,
                        android.R.id.text2});

        // Ajouter l'adapter à la liste
        favoritesView.setAdapter(this.adapter);

        // Écouteur sur chaque élément de la liste
        favoritesView.setOnItemClickListener((adapterView, view, i, l) -> {
            String pseudo = this.favoritesList.get(i).get("pseudo");
            Platform platform = Platform.getEnumByString(this.favoritesList.get(i).get("platform"));
            if (platform != null) {
                String str_platform = platform.getSyntax();
                String url = String.format(BaseActivity.BASE_URL, str_platform, pseudo);
                HttpGetRequest req = new HttpGetRequest(this);
                req.execute(url);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        // A chaque fois qu'on revient sur cette activite
        // s'assurer que les informations de la vue
        // soient synchronisees avec celles de la BDD
        this.refreshList();
    }

    /**
     * Actualise la liste des favoris
     * depuis la BDD
     */
    private void refreshList() {
        // Vider la liste des donnes potentiellement obsoletes
        this.favoritesList.clear();
        Database db = Database.getInstance(this);
        for (Player p : db.getAllFavoris()) {
            // Recuperer tous les favoris depuis la BDD
            // et creer une hashmap pour chacuns
            HashMap<String, String> currPlayer = new HashMap<>(2);
            // Ajouter pseudo et plateforme dans cette map
            currPlayer.put("pseudo", p.getName());
            currPlayer.put("platform", p.getPlatform().getName());
            // Ajouter la map a la liste des favoris
            this.favoritesList.add(currPlayer);
        }
        // Mettre a jour les informations dans la vue via
        // l'adapteur
        this.adapter.notifyDataSetChanged();

    }

}
