package com.p221.grpe.fortnitestats;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;


public class FragmentAdapter extends FragmentPagerAdapter {

    // Liste des fragments
    private final List<Fragment> fragments;

    public FragmentAdapter(FragmentManager fm) {
        super(fm);
        fragments = new ArrayList<>();
    }

    /**
     * Ajoute un fragment a l'activite
     *
     * @param frag fragment a ajouter
     */
    public void addFragment(Fragment frag) {
        this.fragments.add(frag);
    }

    /**
     * Donne un fragment de l'adapteur
     *
     * @param position Position du fragment
     * @return Fragment recupere
     */
    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    /**
     * Donne le nombres de fragments
     * de l'adapteur
     *
     * @return nombre de fragments
     */
    @Override
    public int getCount() {
        return this.fragments.size();
    }
}
