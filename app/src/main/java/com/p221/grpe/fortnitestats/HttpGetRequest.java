package com.p221.grpe.fortnitestats;

import android.content.Intent;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpGetRequest extends AsyncTask<String, Void, String> {

    // Cette classe ne fait que des requetes get
    public static final String METHOD = "GET";

    protected WeakReference<BaseActivity> source_activity;

    public HttpGetRequest(BaseActivity source_activity) {
        this.source_activity = new WeakReference<>(source_activity);
    }

    @Override
    public void onPreExecute() {
        BaseActivity activity = this.source_activity.get();
        if (activity == null || activity.isFinishing()) return;
        activity.showLoading();
    }

    @Override
    protected String doInBackground(String... strings) {
        // L'adresse a GET sera passee en premiere position
        // dans les parametres
        String str_url = strings[0];
        // Pour le moment, on recupere une chaine de caracteres
        String result;
        try {
            // Creer l'url a atteindre
            URL url = new URL(str_url);
            // Creer la connection a cette url
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // Preparer le requete
            conn.setRequestMethod(HttpGetRequest.METHOD);
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestProperty("TRN-Api-Key", Auth.getApiKey());
            conn.connect();
            // Flux de donnee recu
            InputStreamReader input_reader = new InputStreamReader(conn.getInputStream());
            // Lecteur pour lire du texte depuis un flux de donnee
            BufferedReader buf_reader = new BufferedReader(input_reader);
            // Constructeur pour la chaine resultat
            StringBuilder result_builder = new StringBuilder();

            // Iterer sur toutes les lignes recues
            String line;
            while ((line = buf_reader.readLine()) != null) {
                result_builder.append(line);
            }

            // Une fois toute la reponse parcourues, on peut fermer les lecteurs
            input_reader.close();
            buf_reader.close();
            conn.disconnect();

            // Le resultat est le contenu du stringBuilder
            result = result_builder.toString();
        } catch (IOException e) {
            // En cas d'erreur au niveau de la requete GET
            // on retourne null pour pouvoir gerer l'erreur
            // a la fin de la tache
            result = null;
        }
        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        // Recuperer la reference a l'activite
        BaseActivity activity = source_activity.get();
        // Si l'activite n'existe plus, ne rien faire
        if (activity == null || activity.isFinishing()) return;
        // Sinon, cacher la barre de chargement
        activity.hideLoading();
        // Si le GET a leve une erreur, l'afficher dans un toast
        // dans l'activite
        if (result == null) {
            activity.showError("Recherche impossible");
            return;
        }
        // Sinon verifier qu'on ai effectivement trouve un joueur
        JSONObject json;
        try {
            json = new JSONObject(result);
            if (json.has("error") && json.getString("error").equals("Player Not Found")) {
                activity.showError("Joueur introuvable");
                return;
            }
        } catch (JSONException e) {
            activity.showError("Réponse illisible");
        }
        activity.onSuccessfulGet(result);
    }
}
