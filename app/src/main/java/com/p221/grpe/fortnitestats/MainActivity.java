package com.p221.grpe.fortnitestats;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setLayout(R.layout.content_main);
        // Selectionner l'onglet courant dans le drawer
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(true);

        // Ecouter les changement sur le champ de recherche
        Button btn_rechercher = findViewById(R.id.button);
        EditText champ_recherche = findViewById(R.id.seach_editText);
        champ_recherche.addTextChangedListener(new SearchWatcher(btn_rechercher));
        RadioGroup radio_platform = findViewById(R.id.radioGroup);

        btn_rechercher.setOnClickListener(v -> {
            // Quand on clique sur le bouton, construire l'url
            // de la ressource a atteindre en fonction du champ de texte
            // et du bouton radio
            RadioButton selected_platform_radio = findViewById(radio_platform.getCheckedRadioButtonId());
            String player_name = champ_recherche.getText().toString();
            String platform = null;
            switch (selected_platform_radio.getId()) {
                case R.id.radio_pc:
                    platform = "pc";
                    break;
                case R.id.radio_ps4:
                    platform = "psn";
                    break;
                case R.id.radio_xbox:
                    platform = "xbl";
                    break;
            }
            String url = String.format(BaseActivity.BASE_URL, platform, player_name);
            HttpGetRequest req = new HttpGetRequest(this);
            req.execute(url);
        });

    }


}
