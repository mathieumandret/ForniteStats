package com.p221.grpe.fortnitestats;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

public class PickFavoriteActivity extends FavoritesActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.disableDrawer();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.pick_favorite);
        }
    }


    /**
     * Empeche l'acces au drawer
     * et enleve son icone
     */
    protected void disableDrawer() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(null);
    }

    /**
     * Empeche le chargement du menu
     *
     * @param menu menu a bloque
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    /**
     * Methode appelee quand une requete GET lancee
     * par cette activite reussi
     * @param result json non decode a passer a l'activite
     */
    @Override
    public void onSuccessfulGet(String result) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("RAW_DATA", result);
        setResult(PlayerStatsActivity.PICK_FAV, returnIntent);
        finish();
    }
}
