package com.p221.grpe.fortnitestats;

public enum Platform {

    PC("pc", "PC"),
    XBOX_ONE("xbl", "XBox One"),
    PS4("psn", "PlayStation 4");

    private String syntax;
    private String name;

    Platform(String syntax, String name) {
        this.syntax = syntax;
        this.name = name;
    }

    public String getSyntax() {
        return this.syntax;
    }

    public String getName() {
        return this.name;
    }

    public static Platform getEnumByString(String str) {
        switch(str) {
            case "pc":
            case "PC":
                return Platform.PC;
            case "xbl":
            case "XBox One":
                return Platform.XBOX_ONE;
            case "psn":
            case "PlayStation 4":
                return Platform.PS4;
            default:
                return null;
        }
    }

}
