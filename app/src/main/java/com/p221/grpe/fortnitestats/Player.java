package com.p221.grpe.fortnitestats;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class Player {
    private String name;
    private Platform platform;
    private double kd; // Ratio kill/mort
    private double winrate; // Pourcentage de victoire
    private int wins; // Nombre de victoires/top1
    private int games_played; // Nombre de parties jouee
    private int total_kills; // Nombre de kills
    private String time_played; // Temps de jeu total
    private String avg_survival_time; // Duree moyenne d'une partie pour le joueur

    private int xp; // Experience totale gagnee
    private boolean favorite = false;

    public Player(String name, Platform platform) {
        this.name = name;
        this.platform = platform;
    }

    public Player() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public double getKd() {
        return kd;
    }

    public void setKd(double kd) {
        this.kd = kd;
    }

    public double getWinrate() {
        return winrate;
    }

    public void setWinrate(double winrate) {
        this.winrate = winrate;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getGames_played() {
        return games_played;
    }

    public void setGames_played(int games_played) {
        this.games_played = games_played;
    }

    public int getTotal_kills() {
        return total_kills;
    }

    public void setTotal_kills(int total_kills) {
        this.total_kills = total_kills;
    }

    public String getTime_played() {
        return time_played;
    }

    public void setTime_played(String time_played) {
        this.time_played = time_played;
    }

    /**
     * Donne la nombre moyen de kills par partie
     *
     * @return le nombre moyen de mort
     */
    public String getKills_per_match() {
        return String.format(Locale.getDefault(), "%.2f", (double) this.getTotal_kills() / this.getGames_played());
    }

    public String getAvg_survival_time() {
        return avg_survival_time;
    }

    public void setAvg_survival_time(String avg_survival_time) {
        this.avg_survival_time = avg_survival_time;
    }

    public boolean isFavorite() {
        return this.favorite;
    }

    public void setFavorite(boolean fav) {
        this.favorite = fav;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public void parseJSON(String raw_response) throws JSONException {
        JSONObject json = new JSONObject(raw_response);
        // Informations de base
        this.setName(json.getString("epicUserHandle"));
        // Recuperation des statistiques dans un tableau
        // d'objet key => value
        JSONArray stats_array = json.getJSONArray("lifeTimeStats");
        // Plateforme du joueur
        Platform p = Platform.getEnumByString(json.getString("platformName"));
        this.setPlatform(p);
        for (int i = 0; i < stats_array.length(); i++) {
            // Recuperer l'objet courant dans le tableau
            JSONObject stat = stats_array.getJSONObject(i);
            // Associer la valeur de chaque cle au joueur
            switch (stat.getString("key")) {
                case "Matches Played":
                    this.setGames_played(Integer.parseInt(stat.getString("value")));
                    break;
                case "Wins":
                    this.setWins(Integer.parseInt(stat.getString("value")));
                    break;
                case "Win%":
                    String win_percent = stat.getString("value");
                    this.setWinrate(Double.parseDouble(win_percent.substring(0, win_percent.length() - 1)));
                    break;
                case "Kills":
                    this.setTotal_kills(Integer.parseInt(stat.getString("value")));
                    break;
                case "K/d":
                    this.setKd(Double.parseDouble(stat.getString("value")));
                    break;
                case "Avg Survival Time":
                    this.setAvg_survival_time(stat.getString("value"));
                    break;
                case "Time Played":
                    this.setTime_played(stat.getString("value"));
                    break;
                case "Score":
                    String clean_xp = stat.getString("value").replace(",", "");
                    this.setXp(Integer.parseInt(clean_xp));
            }
        }
    }
}
