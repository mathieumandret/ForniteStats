package com.p221.grpe.fortnitestats;

import android.support.v7.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableLayout;

import org.json.JSONException;

import java.util.ArrayList;

public class PlayerStatsActivity extends BaseActivity {

    public static final int PICK_FAV = 1;

    private ViewPager pager;
    private FragmentAdapter adapter;
    private Database db;
    private Player player;
    private Player player2;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setLayout(R.layout.pager_layout);

        Intent incoming_intent = getIntent();
        // Chaine caracteres envoyee par l'activite appelante
        String raw_result = incoming_intent.getStringExtra(MainActivity.ID);
        this.player = new Player();
        try {
            this.player.parseJSON(raw_result);
            ActionBar bar = getSupportActionBar();
            if (bar == null) return;
            bar.setTitle(player.getName());
            this.db = Database.getInstance(this);
            this.player.setFavorite(this.db.playerIsFavorite(this.player));
        } catch (Exception e) {
            finish();
        }

        this.pager = findViewById(R.id.view_pager);
        this.adapter = new FragmentAdapter(getSupportFragmentManager());
        this.setUpPager();
        this.pager.setCurrentItem(0);
        this.updateComparisonFragment();
    }

    /**
     * Initialise le gestionnaire de fragments
     */
    private void setUpPager() {
        this.adapter.addFragment(new StatsFragment());
        this.adapter.addFragment(new CompareFragment());
        this.pager.setAdapter(this.adapter);
    }

    /**
     * Changer le menu pour n'afficher l'icon favoris
     *
     * @param menu menu a changer
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.stats, menu);
        this.menu = menu;
        return true;
    }

    /**
     * Actualise l'icone du joueur en fonction
     * du statut du joueur
     *
     * @param menu menu contenant l'icone
     */
    private void switchIcon(Menu menu) {
        MenuItem item_favori = menu.findItem(R.id.item_favori);
        int icon_id = this.player.isFavorite() ? R.drawable.ic_star_white : R.drawable.ic_star_border;
        item_favori.setIcon(icon_id);
    }

    /**
     * Ajoute le joueur courant en favori
     */
    private void addFavorite() {
        this.db.ajouterFavoris(this.player);
        this.player.setFavorite(true);

    }

    /**
     * Retire le joueur courant des favoris
     */
    private void unFavorite() {
        this.db.removeFavorite(this.player);
        this.player.setFavorite(false);
    }

    /**
     * Change le statut de l'icone
     * favori en fonction du joueur chargé
     *
     * @param menu Menu de l'activite
     * @return true
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        this.switchIcon(menu);
        return true;
    }

    /**
     * Gestion des clics sur le menu
     *
     * @param item item clique
     * @return true
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item_favori) {
            if (!this.player.isFavorite()) {
                this.addFavorite();
            } else {
                this.unFavorite();
            }
            this.switchIcon(this.menu);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Donne le joueur dont les stats sont affichees
     *
     * @return le joueur concerne
     */
    public Player getPlayer() {
        return this.player;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Si on revient d'une requete de selection de favori
        if (resultCode == PlayerStatsActivity.PICK_FAV) {
            // Recuperer les donnees brutes depuis l'intent
            String raw_data = data.getStringExtra("RAW_DATA");
            this.player2 = new Player();
            try {
                this.player2.parseJSON(raw_data);
                this.updateComparisonFragment();
            } catch (JSONException jse) {
                finish();
            }
        }

    }

    private void updateComparisonFragment() {
        TableLayout table = findViewById(R.id.comparison_table);
        CompareFragment frag = (CompareFragment) this.adapter.getItem(1);
        if (frag == null || table == null) return;
        table.setVisibility(View.VISIBLE);
        ArrayList<StatPair> data = frag.generateComparisonArray(this.player, this.player2);
        frag.fillTable(this.player.getName(), this.player2.getName(), data);
    }

}
