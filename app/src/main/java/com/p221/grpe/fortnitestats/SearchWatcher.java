package com.p221.grpe.fortnitestats;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;

public class SearchWatcher implements TextWatcher {

    private Button confirm_button;

    public SearchWatcher(Button btn) {
        this.confirm_button = btn;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        // Apres que le texte du champ de recherce ait ete
        // edite, activer le bouton seulement si ce dernier
        // est non vide
        if (editable.length() > 0) {
            confirm_button.setEnabled(true);
        } else {
            confirm_button.setEnabled(false);
        }
    }
}
