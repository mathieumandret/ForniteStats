package com.p221.grpe.fortnitestats;

/**
 * Represente une paire de statistiques
 * pour 2 joueurs
 * @param <T> Type des statistiques
 */
public class StatPair<T> {

    private T p1_stat;
    private T p2_stat;
    private int view_id; // ID de la vue qui contiendra la paire

    public StatPair(T p1_stat, T p2_stat, int view_id) {
        this.p1_stat = p1_stat;
        this.p2_stat = p2_stat;
        this.view_id = view_id;
    }

    public T getP1_stat() {
        return p1_stat;
    }

    public T getP2_stat() {
        return p2_stat;
    }

    public int getViewId() {
        return this.view_id;
    }

}
