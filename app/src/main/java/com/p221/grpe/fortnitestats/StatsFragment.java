package com.p221.grpe.fortnitestats;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;


public class StatsFragment extends Fragment {

    private Activity parent_activity;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Recuperer l'activite qui a cree le fragment
        this.parent_activity = getActivity();
        if (parent_activity == null) return;
        // Charger les stats du joueur de l'activite.
        this.setViewStats((((PlayerStatsActivity) this.parent_activity).getPlayer()));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_player_stats, container, false);
    }

    /**
     * Affiche les statistiques du joueur dans la vue
     *
     * @param player joueur cible
     */
    private void setViewStats(Player player) {
        // Recuperer les TextViews
        TextView player_top1_tv = this.parent_activity.findViewById(R.id.player_top1);
        TextView player_winrate_tv = this.parent_activity.findViewById(R.id.player_winrate);
        TextView player_kd_tv = this.parent_activity.findViewById(R.id.player_kd);
        TextView player_games_played_tv = this.parent_activity.findViewById(R.id.player_games_played);
        TextView player_total_kills_tv = this.parent_activity.findViewById(R.id.player_total_kills);
        TextView player_play_time_tv = this.parent_activity.findViewById(R.id.player_play_time);
        TextView player_kpm_tv = this.parent_activity.findViewById(R.id.player_kill_per_match);
        TextView player_avg_survival_tv = this.parent_activity.findViewById(R.id.player_avg_survival);
        TextView player_total_xp = this.parent_activity.findViewById(R.id.player_total_xp);
        // Formatter les infos dans les TextViews
        player_top1_tv.setText(String.format(Locale.getDefault(), "%d", player.getWins()));
        player_winrate_tv.setText(String.format(Locale.getDefault(), "%.2f%%", player.getWinrate()));
        player_kd_tv.setText(String.format(Locale.getDefault(), "%.2f", player.getKd()));
        player_games_played_tv.setText(String.format(Locale.getDefault(), "%d", player.getGames_played()));
        player_total_kills_tv.setText(String.format(Locale.getDefault(), "%d", player.getTotal_kills()));
        player_play_time_tv.setText(player.getTime_played());
        player_kpm_tv.setText(player.getKills_per_match());
        player_avg_survival_tv.setText(player.getAvg_survival_time());
        player_total_xp.setText(String.format(Locale.getDefault(), "%d", player.getXp()));
    }
}
