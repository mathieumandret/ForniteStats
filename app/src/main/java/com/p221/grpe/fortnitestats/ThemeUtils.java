package com.p221.grpe.fortnitestats;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class ThemeUtils {

    private static int activeTheme;

    public final static int LIGHT_THEME = 0;
    public final static int DARK_THEME = 1;
    private static boolean init = false;

    /**
     * Change le theme de l'application actif
     *
     * @param activity l'activité concernée par le changement
     */
    public static void switchTheme(Activity activity) {
        // Change le theme actif
        activeTheme = activeTheme == LIGHT_THEME ? DARK_THEME : LIGHT_THEME;
        // Recupere les preferences partagees
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        // Ajoute ou met à jour le thème utilise
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(BaseActivity.PREF_ID_THEME, activeTheme);
        editor.apply();
        editor.commit();
        activity.recreate();
    }

    public static void onActivityCreateSetTheme(Activity activity) {

        // Execute qu'une fois au lancement de l'application
        if (!init) {
            // Recuperation des preferences partagees
            SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
            // Recupération du theme sauvegarde, theme clair s'il n'y en a pas
            activeTheme = sharedPref.getInt(BaseActivity.PREF_ID_THEME, LIGHT_THEME);
            init = true;
        }

        // Application du theme
        switch (activeTheme) {
            case ThemeUtils.LIGHT_THEME:
                activity.setTheme(R.style.AppTheme);
                break;
            case ThemeUtils.DARK_THEME:
                activity.setTheme(R.style.AppThemeDark);
                break;
        }
    }

    public static int getActiveTheme() {
        return activeTheme;
    }

}
